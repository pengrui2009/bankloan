#/usr/bin/python3
import os
import sys
import math
import requests
import pandas as pd

from datetime import datetime


rate_decimal_num = 8  # 利率精度
val_decimal_num = 2  # 数值精度

def get_rate():
    url = 'http://api.xxx.com/rate'
    response = requests.get(url)
    rate = float(response.text)
    return rate

# prinicipal:本金
# annual_rate:年利率
# years:还款年限

def calculate_repayment_pri_int(principal, annual_rate, years):
    monthly_rate = annual_rate / 12
    months = years * 12
    repayment = (principal * monthly_rate * (1 + monthly_rate) ** months) / ((1 + monthly_rate) ** months - 1)
    total_interest = repayment * months - principal
    month_interests = []
    for i in range(1, months) :
        print(i)
        month_interests.append(10000 * principal * monthly_rate * (((1 + monthly_rate) ** months) - ((1 + monthly_rate) ** (i-1))) / ((1 + monthly_rate) ** months-1))
    return (repayment, total_interest, month_interests)

def calculate_repayment_pri(principal, annual_rate, years):
    monthly_rate = annual_rate / 12
    months = years * 12
    repayments = []
    month_interests = []
    repayment_pri = principal / months
    total_interest = 0
    for i in range(months):
        interest = principal * monthly_rate
        total_interest += interest
        principal -= repayment_pri
        repayment = repayment_pri + interest
        repayments.append(repayment)
        # month_interests.append((principal - interest * i) * monthly_rate * 10000)
        month_interests.append((repayment - repayment_pri)*10000)
    return (repayments, total_interest, month_interests)


#######################################
########   等额本息还款公式  ##########
#######################################
class SameInterestLoan:
    """
    计算等额本息方式还款,所需的参数
    :param capital: 本金
    :param monthly_interest_rate: 月利率
    :param start_date: 还款起始日期
    :param total_months: 总还款月数
    """
    def __init__(self, capital, monthly_interest_rate, start_date, total_months) -> None:
        self.capital = capital
        self.monthly_interest_rate = monthly_interest_rate
        self.start_date = start_date
        self.total_months = total_months
        self.month_payments = []
        # 计算基本信息
        self.cal_basic(monthly_interest_rate, total_months, capital)

    def cal_basic(self, monthly_interest_rate, total_months, capital):
        # 计算基础信息，包括月供、累计还款额，累计利息
        # 参数分别为月利率，总期数，本金        
        total_per_month = capital * (1+monthly_interest_rate)** total_months * monthly_interest_rate / ((1+monthly_interest_rate)**total_months-1)  # 月供
        cumulative_pay = total_months * capital * (1+monthly_interest_rate)** total_months * monthly_interest_rate / ((1+monthly_interest_rate)**total_months-1)  # 累计还款，包括本金和利息
        cumulative_interest = cumulative_pay - capital # 累计还利息
        left_capital = capital
        self.month_payments.clear()

        for index in range(total_months) :
            # 还款月份，月供，每月还款本金，每月还款利息，剩余本金
            month_payment = {'month': 0, 'total': 0.0, 'capital': 0.0, 'interest': 0.0, 'left_capital': 0.0}

            month_payment['month'] = index
            month_payment['total'] = round(total_per_month, val_decimal_num)
            month_interest = capital * monthly_interest_rate * (((1 + monthly_interest_rate) ** total_months) - 
                               ((1 + monthly_interest_rate) ** (index-1))) / ((1 + monthly_interest_rate) ** total_months-1)
            month_payment['capital'] = round(total_per_month-month_interest, val_decimal_num)
            month_payment['interest'] = round(month_interest, val_decimal_num)
            month_payment['left_capital'] = round(left_capital-total_per_month+month_interest, val_decimal_num)
            left_capital -= (total_per_month-month_interest)
            self.month_payments.append(month_payment)

        # print('same interest:')
        # for payment in self.month_payments :
        #     print('{} {} {} {} {}'.format(
        #         payment['month'], payment['total'], payment['capital'], payment['interest'], payment['left_capital']))
        
        return (round(total_per_month, val_decimal_num), round(cumulative_pay,val_decimal_num), round(cumulative_interest,val_decimal_num))

    def cal_per_month_by_left_capital(self, monthly_interest_rate, left_capital, total_per_month):
        # 逐月计算月还款利息等信息，包括每月还款利息、每月还款本金、剩余本金
        # 输入参数为月还款利息，剩余本金、月供
        interest_month = left_capital * monthly_interest_rate
        capital_month = total_per_month - interest_month
        if capital_month > left_capital:
            # 最后一个月，本金可能小于基于月供计算的数
            capital_month = left_capital
        # print(left_capital, capital_month, left_capital - capital_month)
        return (round(interest_month, val_decimal_num), round(capital_month, val_decimal_num), round(left_capital - capital_month, val_decimal_num))

    def cal_delta_months(self, start_date, event_happen_date):
        # 计算某个时间发生的日期相较于起始日期的月份偏移数
        delta_year = event_happen_date.year - start_date.year
        delta_month = event_happen_date.month - start_date.month
        delta_day = event_happen_date.day - start_date.day
        res = delta_year * 12 + delta_month
        if delta_day < 0:
            res -= 1
        return res

    def next_month(self, current_date):
        # 获取下个月的datetime对象
        year = current_date.year
        month = current_date.month
        if month == 12:
            year += 1
            month = 1
        else:
            month += 1
        return datetime(year, month, current_date.day)  

    """
    月供不变,计算新的期限,所需的参数
    :param m_pay_fixed: 月供金额
    :param current_monthly_interest_rate: 当前月利率
    :param total_left_capital:剩余本金
    :return:返回每期的利息，当期本金和期末本金
    """
    def prepayment_decrease_duration(self, m_pay_fixed, current_monthly_interest_rate, total_left_capital):
        # 期限变短，月供不变,计算新的期限
        prepayment_month = 0
        for index in range(1, 1000) :
            result = self.cal_basic(current_monthly_interest_rate, index, total_left_capital)
            if result[0] <= m_pay_fixed :
                prepayment_month = index
                break
        #prepayment_month = (total_left_capital * current_monthly_interest_rate * (1+current_monthly_interest_rate) ** )

        return prepayment_month #datetime(year, month, current_date.day)  

    def cal_prepapment_interest(self, original_captical, prepay_amount, monthly_interest_rate, last_pay_date, prepay_date):
        # 前提：上次付款日和提前还款日差值不超过30天
        # 计算提前还款当月应还利息
        if prepay_date.month == last_pay_date.month:
            # 同一个月
            duration_before = prepay_date.day - last_pay_date.day
            duration_after = 30 - last_pay_date.day + last_pay_date.day  # 提前还款后，下次还款日相同  = 30-duration_before
        else:
            # 不同月，则一个月按照30天计算
            duration_before = 30 - last_pay_date.day + prepay_date.day
            duration_after = last_pay_date.day - prepay_date.day # 日期算头不算尾   = 30-duration_before
        # print(duration_before, duration_after)
        return round((original_captical * duration_before + (original_captical - prepay_amount) * duration_after)  * monthly_interest_rate / 30, val_decimal_num)

    def view_all_pay_info(self, change_event_list):        
        # 主函数。根据起始月利息，期数、本金、开始还款日期、事件发生列表等，生成每月还款信息表
        monthly_interest_rate = self.monthly_interest_rate
        total_months = self.total_months 
        capital = self.capital
        start_date = self.start_date

        current_monthly_interest_rate = monthly_interest_rate
        m_pay_fixed,_,_ = self.cal_basic(monthly_interest_rate, total_months, capital)  # 总的还款信息：月供、累计还款、累计还利息
        current_date = start_date
        full_info_list = []
        total_left_capital = capital # 剩余本金
        # 计算事件发生改变的月
        has_event_month_indexes = []
        for event in change_event_list:
            has_event_month_indexes.append(self.cal_delta_months(start_date, event['happen_date']) + 1 + 1)  # +1是从第一个月开始计算，再+1是事件生效月
        event_index = 0
        print(has_event_month_indexes)
        for m in range(1, total_months+1):
            if m > total_months:
                # 总还款时间有可能变化
                break
            # 处理事件
            description = ''
            changed_interest = 0 # 如果提前还款，可能会导致当月利息变化
            if m in has_event_month_indexes:
                for n in range(has_event_month_indexes.count(m)):
                    # 可能发生多个事件
                    event = change_event_list[event_index]
                    event_index += 1
                    if event['event_type'] == 0:
                        # 利率改变
                        current_monthly_interest_rate = round(event['new_yearly_interest_rate'] / 12, rate_decimal_num)  # 保留x位小数
                        m_pay_fixed,_,_ = self.cal_basic(current_monthly_interest_rate, total_months-m+1, total_left_capital)  # 更新总还款信息
                        description += '利率变化:' + str(current_monthly_interest_rate) + ' '
                    elif event['event_type'] == 1:
                        # 提前还款
                        changed_interest = self.cal_prepapment_interest(total_left_capital, event['amount'], current_monthly_interest_rate, last_date, event['happen_date'])
                        total_left_capital -= event['amount']
                        if event['prepay_type'] == 0:
                            # 期限不变，月供减少
                            m_pay_fixed,_,_ = self.cal_basic(current_monthly_interest_rate, total_months-m+1, total_left_capital)  # 更新总还款信息
                            description += '提前还款:' + str(event['amount'] / 10000) + '万 '
                        elif event['prepay_type'] == 1:
                            # 期限变短，月供不变
                            # 计算期限
                            new_duration = self.prepayment_decrease_duration(m_pay_fixed, current_monthly_interest_rate, total_left_capital)
                            print(new_duration, m)
                            total_months = new_duration + m -1
                            description += '提前还款:' + str(event['amount'] / 10000) + '万 '
                        else:
                            # 月供减少，同时期限变短
                            total_months = event['new_total_months']
                            m_pay_fixed,_,_ = self.cal_basic(current_monthly_interest_rate, total_months-m+1, total_left_capital)  # 更新总还款信息
                            description += '提前还款:' + str(event['amount'] / 10000) + '万 '
            m_interest, m_capital, total_left_capital = self.cal_per_month_by_left_capital(current_monthly_interest_rate, total_left_capital, m_pay_fixed) # 每月还利息、每月还本金、剩余本金
            m_pay_total = round(m_pay_fixed, val_decimal_num) # 当月还款总额
            if changed_interest != 0:
                # 利息变化
                m_interest = changed_interest
                m_pay_total = m_capital + changed_interest
            full_info_list.append((current_date, m, m_pay_total, m_interest, m_capital, total_left_capital, description)) 
            last_date = current_date                                                 
            current_date = self.next_month(current_date)
        full_info_list_df = pd.DataFrame(full_info_list, columns=['日期', '月数', '月供', '还利息', '还本金', '剩余本金', '事件描述'])
        return full_info_list_df, full_info_list

#######################################
########   等额本金还款公式  ##########
#######################################
class SameCapitalLoan:
    def __init__(self, capital, monthly_interest_rate, start_date, total_months) -> None:
        self.capital = capital
        self.monthly_interest_rate = monthly_interest_rate
        self.start_date = start_date
        self.total_months = total_months
        self.month_payments = []

        # 计算基本信息
        self.cal_basic(monthly_interest_rate, total_months, capital)
    """
    计算基础信息，包括月供、累计还款额，累计利息,所需的参数
    :param monthly_interest_rate: 月利率
    :param total_months: 总还款月数
    :param capital:本金
    :return:返回 {月供、累计还款额，累计利息}
    """
    def cal_basic(self, monthly_interest_rate, total_months, capital):
        # 计算基础信息，包括月供、累计还款额，累计利息
        # 参数分别为月利率，总期数，本金
        total_per_month = 0   # 月供
        cumulative_pay = 0      # 累计还款，包括本金和利息
        cumulative_interest = 0 # 累计还利息        
        self.month_payments.clear()

        repayment_pri = capital / total_months
        
        left_capital = capital
        for index in range(1, total_months+1):
            # 还款月份，月供，每月还款本金，每月还款利息，剩余本金
            month_payment = {'month': 0, 'total': 0.0, 'capital': 0.0, 'interest': 0.0, 'left_capital': 0.0}

            interest = capital * monthly_interest_rate
            cumulative_interest += interest
            capital -= repayment_pri
            repayment = repayment_pri + interest
            if total_per_month == 0:
                total_per_month = repayment
            # month_interests.append((principal - interest * i) * monthly_rate * 10000)
            # month_interests.append((repayment - repayment_pri)*10000)            
            
            month_payment['month'] = index
            month_payment['total'] = round(repayment,val_decimal_num)
            month_payment['capital'] = round(repayment_pri, val_decimal_num)
            month_payment['interest'] = round(repayment - repayment_pri, val_decimal_num)
            month_payment['left_capital'] = round(left_capital - repayment_pri, val_decimal_num)
            left_capital -= repayment_pri
            self.month_payments.append(month_payment)

        # print('same capital:')
        # for payment in self.month_payments:
        #     print('{} {} {} {} {}'.format(
        #         payment['month'], payment['total'], payment['capital'], payment['interest'], payment['left_capital']))
            
        cumulative_pay = capital + cumulative_interest 

        return (round(total_per_month, val_decimal_num) , round(cumulative_pay,val_decimal_num), round(cumulative_interest,val_decimal_num))

    """
    逐月计算月还款利息等信息，包括每月还款利息、每月还款本金、剩余本金,所需的参数
    :param month: 还款月份
    :param monthly_interest_rate: 月利率
    :param left_capital: 剩余本金
    :param total_months: 月供
    :return:返回 {每月还款利息、每月还款本金、剩余本金}
    """
    def cal_per_month_by_left_capital(self, month_num, monthly_interest_rate, total_per_month, left_capital):
        # 逐月计算月还款利息等信息，包括每月还款利息、每月还款本金、剩余本金
        # 输入参数为月还款利息，剩余本金、月供
        # for index in range(month_num) :
        # interest_month = left_capital * monthly_interest_rate
        # capital_month = total_per_month - interest_month
        # if capital_month > left_capital:
            # 最后一个月，本金可能小于基于月供计算的数
            # capital_month = left_capital
        # print(left_capital, capital_month, left_capital - capital_month)
        interest_month = self.month_payments[0]['interest']
        capital_month = self.month_payments[0]['capital']
        if capital_month > left_capital:
            # 最后一个月，本金可能小于基于月供计算的数
            capital_month = left_capital
        # left_capital = self.month_payments[0]['']
        del self.month_payments[0]
        
        return (round(interest_month, val_decimal_num), round(capital_month, val_decimal_num), round(left_capital - capital_month, val_decimal_num))

    """
    计算某个时间发生的日期相较于起始日期的月份偏移数
    :return:返回 {月份偏移数}
    """
    def cal_delta_months(self, start_date, event_happen_date):
        # 计算某个时间发生的日期相较于起始日期的月份偏移数
        delta_year = event_happen_date.year - start_date.year
        delta_month = event_happen_date.month - start_date.month
        delta_day = event_happen_date.day - start_date.day
        res = delta_year * 12 + delta_month
        if delta_day < 0:
            res -= 1
        return res

    def next_month(self, current_date):
        # 获取下个月的datetime对象
        year = current_date.year
        month = current_date.month
        if month == 12:
            year += 1
            month = 1
        else:
            month += 1
        return datetime(year, month, current_date.day)  

    """
    月供不变,计算新的期限,所需的参数
    :param m_pay_fixed: 月供金额
    :param current_monthly_interest_rate: 当前月利率
    :param total_left_capital:剩余本金
    :return:返回每期的利息，当期本金和期末本金
    """
    def prepayment_decrease_duration(self, m_pay_fixed, current_monthly_interest_rate, total_left_capital):
        # 期限变短，月供不变,计算新的期限
        prepayment_month = 0
        for index in range(1, 1000) :
            result = self.cal_basic(current_monthly_interest_rate, index, total_left_capital)
            if result[0] <= m_pay_fixed :
                prepayment_month = index
                break
        #prepayment_month = (total_left_capital * current_monthly_interest_rate * (1+current_monthly_interest_rate) ** )

        return prepayment_month #datetime(year, month, current_date.day)  

    def cal_prepapment_interest(self, original_captical, prepay_amount, monthly_interest_rate, last_pay_date, prepay_date):
        # 前提：上次付款日和提前还款日差值不超过30天
        # 计算提前还款当月应还利息
        if prepay_date.month == last_pay_date.month:
            # 同一个月
            duration_before = prepay_date.day - last_pay_date.day
            duration_after = 30 - last_pay_date.day + last_pay_date.day  # 提前还款后，下次还款日相同  = 30-duration_before
        else:
            # 不同月，则一个月按照30天计算
            duration_before = 30 - last_pay_date.day + prepay_date.day
            duration_after = last_pay_date.day - prepay_date.day # 日期算头不算尾   = 30-duration_before
        # print(duration_before, duration_after)
        return round((original_captical * duration_before + (original_captical - prepay_amount) * duration_after)  * monthly_interest_rate / 30, val_decimal_num)

    def view_all_pay_info(self, change_event_list):
        # 主函数。根据起始月利息，期数、本金、开始还款日期、事件发生列表等，生成每月还款信息表
        monthly_interest_rate = self.monthly_interest_rate
        total_months = self.total_months
        capital =  self.capital
        start_date = self.start_date
        
        current_monthly_interest_rate = monthly_interest_rate
        m_pay_fixed,_,_ = self.cal_basic(monthly_interest_rate, total_months, capital)  # 总的还款信息：月供、累计还款、累计还利息
        current_date = start_date
        full_info_list = []
        total_left_capital = capital # 剩余本金
        # 计算事件发生改变的月
        has_event_month_indexes = []
        for event in change_event_list:
            has_event_month_indexes.append(self.cal_delta_months(start_date, event['happen_date']) + 1 + 1)  # +1是从第一个月开始计算，再+1是事件生效月
        event_index = 0
        print(has_event_month_indexes)
        for m in range(total_months):
            if m > total_months:
                # 总还款时间有可能变化
                break
            # 处理事件
            description = ''
            changed_interest = 0 # 如果提前还款，可能会导致当月利息变化
            if m in has_event_month_indexes:
                for n in range(has_event_month_indexes.count(m)):
                    # 可能发生多个事件
                    event = change_event_list[event_index]
                    event_index += 1
                    if event['event_type'] == 0:
                        # 利率改变
                        current_monthly_interest_rate = round(event['new_yearly_interest_rate'] / 12, rate_decimal_num)  # 保留x位小数
                        m_pay_fixed,_,_ = self.cal_basic(current_monthly_interest_rate, total_months-m+1, total_left_capital)  # 更新总还款信息
                        description += '利率变化:' + str(current_monthly_interest_rate) + ' '
                    elif event['event_type'] == 1:
                        # 提前还款
                        changed_interest = self.cal_prepapment_interest(total_left_capital, event['amount'], current_monthly_interest_rate, last_date, event['happen_date'])
                        total_left_capital -= event['amount']
                        if event['prepay_type'] == 0:
                            # 期限不变，月供减少
                            m_pay_fixed,_,_ = self.cal_basic(current_monthly_interest_rate, total_months-m+1, total_left_capital)  # 更新总还款信息
                            description += '提前还款:' + str(event['amount'] / 10000) + '万 '
                        elif event['prepay_type'] == 1:
                            # 期限变短，月供不变
                            # 计算期限
                            new_duration = self.prepayment_decrease_duration(m_pay_fixed, current_monthly_interest_rate, total_left_capital)
                            print(new_duration, m)
                            total_months = new_duration + m -1
                            description += '提前还款:' + str(event['amount'] / 10000) + '万 '
                        else:
                            # 月供减少，同时期限变短
                            total_months = event['new_total_months']
                            m_pay_fixed,_,_ = self.cal_basic(current_monthly_interest_rate, total_months-m+1, total_left_capital)  # 更新总还款信息
                            description += '提前还款:' + str(event['amount'] / 10000) + '万 '
            m_interest, m_capital, total_left_capital = self.cal_per_month_by_left_capital(m, current_monthly_interest_rate, m_pay_fixed, total_left_capital) # 每月还利息、每月还本金、剩余本金
            m_pay_total = round(m_pay_fixed, val_decimal_num) # 当月还款总额
            if changed_interest != 0:
                # 利息变化
                m_interest = changed_interest
                m_pay_total = m_capital + changed_interest
            full_info_list.append((current_date, m, m_pay_total, m_interest, m_capital, total_left_capital, description)) 
            last_date = current_date                                                 
            current_date = self.next_month(current_date)
        full_info_list_df = pd.DataFrame(full_info_list, columns=['日期', '月数', '月供', '还利息', '还本金', '剩余本金', '事件描述'])
        return full_info_list_df, full_info_list

def main(argv):

    # print('{}'.format(argv))
    bussiness_principal = 190
    bussiness_rate = 4.2 * 0.01

    commonfund_principal = 90
    commonfund_rate = 3.1 * 0.01

    ###############################################################################
    if 1 :
        # 基础参数
        capital = 1900000 # 本金
        months = 20 * 12 # 月数
        start_date = datetime(2023,9,24)
        year_interest_rate = 3.8 / 100
        monthly_interest_rate = year_interest_rate/12
        change_event_list = []  # 注意，这里按照事件的发生时间由远及近
        change_event_list.append(
            {
                'happen_date': datetime(2024,8,24),
                'event_type': 1,  # 0-->利率改变，1-->提前还款
                'amount': 200000,
                'prepay_type': 1  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
            })
        change_event_list.append(
            {
                'happen_date': datetime(2025,8,24),
                'event_type': 1,  # 0-->利率改变，1-->提前还款
                'amount': 200000,
                'prepay_type': 1,  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
            })
        change_event_list.append(
            {
                'happen_date': datetime(2026,8,24),
                'event_type': 1,  # 0-->利率改变，1-->提前还款
                'amount': 200000,
                'prepay_type': 1  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
            })
        change_event_list.append(
            {
                'happen_date': datetime(2027,8,24),
                'event_type': 1,  # 0-->利率改变，1-->提前还款
                'amount': 200000,
                'prepay_type': 1,  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
            })        
        sameinterestloan_test = SameInterestLoan(capital, monthly_interest_rate, start_date, months)
        res = sameinterestloan_test.view_all_pay_info(change_event_list)
        res[0].to_csv('house_sameinterest_loan.csv', index=False)

        total_interest = 0.0
        for item in res[1] :
            total_interest += item[3]
        print('same interest total_interest:{}'.format(total_interest))
    #     # sameinterestloan_result = sameinterestloan_test.cal_basic(3.8/100/12, 12 *30, 1900000)
    #     # print('{} {} {}'.format(sameinterestloan_result[0], sameinterestloan_result[1], sameinterestloan_result[2]))    
    
    if 1 :
       # 基础参数
        capital = 1900000 # 本金
        months = 25 * 12 # 月数
        start_date = datetime(2023,9,24)
        year_interest_rate = 3.8 / 100
        monthly_interest_rate = year_interest_rate/12
        change_event_list = []  # 注意，这里按照事件的发生时间由远及近
        change_event_list.append(
            {
                'happen_date': datetime(2024,9,24),
                'event_type': 1,  # 0-->利率改变，1-->提前还款
                'amount': 200000,
                'prepay_type': 1  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
            })
        change_event_list.append(
            {
                'happen_date': datetime(2025,9,24),
                'event_type': 1,  # 0-->利率改变，1-->提前还款
                'amount': 200000,
                'prepay_type': 1,  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
            })
        change_event_list.append(
            {
                'happen_date': datetime(2026,9,24),
                'event_type': 1,  # 0-->利率改变，1-->提前还款
                'amount': 200000,
                'prepay_type': 1  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
            })
        change_event_list.append(
            {
                'happen_date': datetime(2027,9,24),
                'event_type': 1,  # 0-->利率改变，1-->提前还款
                'amount': 200000,
                'prepay_type': 1,  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
            })
        samecapitalloan_test = SameCapitalLoan(capital, monthly_interest_rate, start_date, months)
        res = samecapitalloan_test.view_all_pay_info(change_event_list)
        res[0].to_csv('house_samecapital_loan.csv', index=False)
        
        total_interest = 0.0
        for item in res[1] :
            total_interest += item[3]
        print('same capital total interest:{}'.format(total_interest))
        # samecapitalloan_result = samecapitalloan_test.cal_basic(3.8/100/12, 12 *30, 1900000)
        # print('{} {} {}'.format(samecapitalloan_result[0][0], samecapitalloan_result[1], samecapitalloan_result[2]))
    ###############################################################################
    # 基础参数
    # capital = 3040000 # 本金
    # months = 25 * 12 # 月数
    # start_date = datetime(2021,7,24)
    # year_interest_rate = 5.2 / 100
    # change_event_list = []  # 注意，这里按照事件的发生时间由远及近
    # change_event_list.append(
    #     {
    #         'happen_date': datetime(2022,7,1),
    #         'event_type': 0,  # 0-->利率改变，1-->提前还款
    #         'new_yearly_interest_rate': 0.05
    #     })
    # change_event_list.append(
    #     {
    #         'happen_date': datetime(2023,3,9),
    #         'event_type': 1,  # 0-->利率改变，1-->提前还款
    #         'amount': 200000,
    #         'prepay_type': 0,  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
    #     })
    # change_event_list.append(
    #     {
    #         'happen_date': datetime(2023,6,24),
    #         'event_type': 0,  # 0-->利率改变，1-->提前还款
    #         'new_yearly_interest_rate': 0.0475
    #     })
    # change_event_list.append(
    #     {
    #         'happen_date': datetime(2023,7, 22),
    #         'event_type': 1,  # 0-->利率改变，1-->提前还款
    #         'amount': 200000,
    #         'prepay_type': 1,  # 0:期限不变，月供变少；1:月供不变，期限变短; 2:月供减少，同时期限变短
    #     })


    # res = view_all_pay_info(round(year_interest_rate / 12, rate_decimal_num), months, capital, start_date, change_event_list)
    # res.to_csv('house_loan.csv', index=False)
    ###############################################################################
    # print('same load pay:')
    # result00 = calculate_repayment_pri_int(bussiness_principal, bussiness_rate, 15)
    # # print('{} {}'.format(result00[0] * 10000, result00[1]))
    # result01 = calculate_repayment_pri_int(commonfund_principal, commonfund_rate, 15)
    # print('{} {} {} '.format((result00[0] * 10000 + result01[0] * 10000), 
    #     (result00[1] + result01[1]), (result00[2][0] + result01[2][0])))

    # total_pay = 0
    # left_total_money = (result00[0] * 10000 + result01[0] * 10000) * 15 * 12
    # for index in range(15, 0, -1):
    #     left_total_money 


    # print('some money pay:')
    # result10 = calculate_repayment_pri(bussiness_principal, bussiness_rate, 20)
    # # print('{} {}'.format(result[0][0] * 10000, result[1]))

    # result11 = calculate_repayment_pri(commonfund_principal, commonfund_rate, 20)
    # print('{} {} {}'.format((result10[0][0] * 10000 + result11[0][0] * 10000), 
    #     (result10[1]+result11[1]), (result10[2][0] + result11[2][0])))

if __name__ == '__main__':
    main(sys.argv[1:])